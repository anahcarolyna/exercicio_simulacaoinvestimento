package br.com.itau;

public class Cliente {
    private String nome;
    private String cpf;

    public Cliente(String cpf){
       ValidarDadosCadastrais(cpf);
    }
     public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

     public String getNome(){
        return nome;
     }

    public void ValidarDadosCadastrais(String cpf){
        if( cpf.equals("34627263821")){
            this.setNome("Carol");
            this.setCpf("34627263821");
        }
        else
            Impressora.imprimir("Cliente não cadastrado");
    }
}
