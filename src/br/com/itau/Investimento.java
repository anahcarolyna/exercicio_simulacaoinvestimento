package br.com.itau;

import java.text.DecimalFormat;

public class Investimento {
    private double valorInvestido;
    private int quantidadeMeses;
    private static final double taxajuros = 0.7/100;

    public Investimento(double valorInvestido, int quantidadeMeses){
        this.valorInvestido = valorInvestido;
        this.quantidadeMeses = quantidadeMeses;
    }

    public void setValorInvestido(double valorInvestido) {
        this.valorInvestido = valorInvestido;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Resultado getSoma() {
        for (int tempo1 = 1; tempo1 <= this.quantidadeMeses; tempo1++) {
            this.valorInvestido += (taxajuros * this.valorInvestido);
        }
        DecimalFormat fmt = new DecimalFormat("0.00");
        return new Resultado(fmt.format(this.valorInvestido));

    }
}
