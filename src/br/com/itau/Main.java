package br.com.itau;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Impressora.imprimir("Por favor, digite seu CPF:");

        Cliente cliente = new Cliente(input.next());

        Impressora.imprimir("Seja bem vinda " + cliente.getNome() + ". Escolha o valor a ser investido no fundo XPTO:");
        double valorInvestido = input.nextDouble();

        Impressora.imprimir("Digite o tempo em meses:");
        int quantidadeMeses = input.nextInt();

        Investimento invest = new Investimento(valorInvestido, quantidadeMeses);

        Impressora.imprimir(invest.getSoma());
    }
}
