package br.com.itau;

public class Resultado {
   private String montanteFinal;

   public Resultado(String valorFinal){
       this.montanteFinal = valorFinal;
   }

    public String getMontanteFinal() {
        return montanteFinal;
    }
}
